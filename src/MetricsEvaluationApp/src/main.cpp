#include <string>
#include <iostream>
#include "SQLDataset.h"
#include "PerfEvalOTAMetricsCalculator.h"
#include <fstream>
#include "CLEARMetricsCalculator.h"
#include "StringHelpers.h"
#include "Logger.h"
#include "MatrixIO.h"
void removeOutOfRangeTimestamp(Dataset* gt, Dataset* system);
int main(int argNumber, char* argsString[])
{
	bool prompt = true;
	if(argNumber >= 5)
	{
		std::string GTPath(argsString[1]);
		std::string experimentalPath(argsString[2]);
		SQLDataset* groundTruth = new SQLDataset();
		SQLDataset* system = new SQLDataset();

		if(argNumber>=6)
		{		
			std::string homographyPath(argsString[5]);
			cv::Mat homography;
			if(homographyPath != "none")
			{
				Utils::IO::LoadMatrix<double>(homographyPath, homography);
				//cv::transpose(homography, homography);
				cv::invert(homography,homography);
				system->setHomography(homography);
			}
		}

		groundTruth->Load(GTPath);
		system->Load(experimentalPath);
		removeOutOfRangeTimestamp(groundTruth, system);
		if(!groundTruth->isLoaded())
			std::cout << "Unable to load " << GTPath << std::endl;
		if(!system->isLoaded())
			std::cout << "Unable to load " << experimentalPath << std::endl;
		


		PerfEvalOTAMetricsCalculator mc(groundTruth, system);
		PerfEvalOTAMetrics peotaMetrics = mc.calculateMetrics();
		peotaMetrics.displayResults();

		MeasureDistType type;
		std::string typeStr(argsString[3]);
		if(typeStr == "ABSDIST")
			type = ABSOLUTEDIST;
		else if(typeStr  == "BBOVERLAP")
			type = BOUNDINGBOXOVERLAP;

		double threshold;
		std::string thresholdStr(argsString[4]);
		if(!Utils::String::StringToType<double>(thresholdStr, threshold))
		{
			LOGERROR(argsString[4] << " should be a number");
		}
	

		
		CLEARMetricsCalculator cmc(groundTruth, system, threshold, type);
		CLEARMetrics cMetrics = cmc.calculateMetrics();
		cMetrics.displayResults();


		
		
		
		if(argNumber >= 7)
		{
			std::string writeResultPath(argsString[6]);
			std::ofstream outFile(writeResultPath.c_str());
			outFile << 	peotaMetrics.serialize();
			outFile << cMetrics.serialize();
			outFile.close();

			if(argNumber >= 8)
			{
				std::string noPromptStr(argsString[7]);
				if(noPromptStr == "noprompt")
					prompt = false;
			}
		}
		

		delete groundTruth;
		delete system;
	}
	else
		std::cout << "Not enough args. You must put the input path for your GT and the input path for you system track\nex:\"groundtruth path\" \"algorithm path\" \"association mode\" \"association threshold\" [write] [noprompt]\n\n\
-groundtruth path: The full path to the ground truth .sqlite database\n\
-algorithm path: The full path to the algorithm resulting .sqlite database\n\
-association mode: ABSDIST  if you want to associate based on the absolute distance of the centroid between bounding boxes\n\
                   BBOVERLAP        if you want to associate based on the total overlap of bounding box\n\
-association threshold: For ABSDIST, use a pixel number (ex: 70 would mean the maximum association distance between two box is 70 px).\n\
                            BBOVERLAP, use a value between 0 and 1 (ex: 0.5 would mean you need an overlap of at least 50% between to bounding box to make an association)\n\
-write (optional): If you put a file path here, the results will be saved in a file in an easy text format to allows result compilation\n\
-noprompt (optional): If this argument is noprompt, it will not display a prompt waiting for keys\n";

					
	if(prompt)
	{
		std::cin.sync();
		std::cout << std::endl << "Press ENTER to continue..." << std::endl;
		std::cin.get();
	}

}


void removeOutOfRangeTimestamp(Dataset* gt, Dataset* system)
{
	const std::vector<SceneObjectWithObservation*>& gtObjectList = gt->getSceneObjectList();
	const std::vector<SceneObjectWithObservation*>& systemObjectList = system->getSceneObjectList();
	//We remove observations outside the gt timestamp range
	unsigned int minTimestamp = -1;
	unsigned int maxTimestamp = 0;
	for(unsigned int i = 0; i < gtObjectList.size(); ++i)
	{
		const std::vector<Observation*>& obs = gtObjectList.at(i)->mObservations;
		for(auto oIt = obs.begin(); oIt != obs.end(); ++oIt)
		{
			unsigned int timestamp = (*oIt)->mTimestamp;
			if(timestamp > maxTimestamp)
				maxTimestamp = timestamp;
			if(timestamp < minTimestamp)
				minTimestamp = timestamp;
		}
	}
	std::set<int> timestampToRemove;
	
	bool removeObs = false;
	for(auto it = systemObjectList.begin(); it != systemObjectList.end(); ++it)
	{
		std::vector<Observation*>& obs = (*it)->mObservations;
		for(auto oIt = obs.begin(); oIt != obs.end(); ++oIt)
		{
			if((*oIt)->mTimestamp > maxTimestamp || (*oIt)->mTimestamp < minTimestamp)
			{
				timestampToRemove.insert((*oIt)->mTimestamp);
				removeObs = true;
			}
		}		
	}
	system->removeTimestamp(timestampToRemove);
	if(removeObs)
		std::cout << "Some observation in the dataset were outside the ground truth annotation range and they were not considered\n";

}