#include "TrafficIntelligenceSQLImporter.h"
#include "SQLiteManager.h"
#include "AppContext.h"
#include "SceneObject.h"
#include <map>
#include <iostream>
#include "ObjectObservation.h"
#include "DrawableSceneView.h"
#include "InputVideoFileModule.h"
#include "OpenCVHelpers.h"
TrafficIntelligenceSQLImporter::TrafficIntelligenceSQLImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "Traffic Intelligence SQL database", "*.sqlite", "TISQL" , GTImporter::FILE, videoWidth, videoHeight)
{

}


TrafficIntelligenceSQLImporter::~TrafficIntelligenceSQLImporter()
{

}


void TrafficIntelligenceSQLImporter::LoadDatabase()
{
	//Load homography matrix



	typedef std::map<int, cv::Point> PointList; 
	typedef std::map<int, PointList> TrajectoryMap;
	typedef std::map<int, std::vector<PointList>> ObjectTrajectoryMap;

	TrajectoryMap trajectories;
	
	bool success = true;
	SQLiteManager sqlMgr(getLoadPath().toStdString());
	if(sqlMgr.isConnected())
	{
		updatePercent(10);
		// 1) On charge les trajectoires
		std::vector<std::vector<std::string>> positions;
		success &= sqlMgr.executeStatementGetArray("SELECT trajectory_id, frame_number, x_coordinate, y_coordinate FROM positions", positions);
		std::map<int, bool> trajIdUsedMap;
		//cv::Mat im = cv::imread("D:\\1750.png");
		if(success && !positions.empty() && positions[0].size() == 4)
		{
			for (unsigned int row = 0; row <positions.size(); ++row)
			{
				updatePercent(10 + 40*(float)row/positions.size());
				int trajId, frame_number;
				double xPos, yPos;
				success &= Utils::String::StringToType(positions[row][0], trajId);
				success &= Utils::String::StringToType(positions[row][1], frame_number);
				success &= Utils::String::StringToType(positions[row][2], xPos);
				success &= Utils::String::StringToType(positions[row][3], yPos);
				trajIdUsedMap[trajId] = false;
				PointList& pl = trajectories[trajId];

				cv::Point p(xPos, yPos);
				if(!getHomography().empty())
				{

					std::vector<cv::Point2f> beforeHomography;
					beforeHomography.push_back(cv::Point2f(xPos, yPos));

					std::vector<cv::Point2f> afterHomography;
					cv::perspectiveTransform(beforeHomography,afterHomography, getHomography());

					pl[frame_number] = afterHomography[0];
				}
				else
					pl[frame_number] = p;
				
				//cv::Point2f(xPos, yPos);
				/*if(frame_number == 1766)
				{
				
					cv::circle(im, afterHomography[0], 3, cv::Scalar(255,0,0), 3);
				}*/
			
			}
		}
		//cv::imshow("Test", im);
		//cv::imwrite("1750_points.png", im);

		
		LOGASSERT(success, "Trajectories failed loading");
		//On charge les objets
		ObjectTypeManager& otm = getAppContext()->getObjectTypeManager();
		std::map<int, std::string> objectToTypeMap;
		std::vector<std::vector<std::string>> objects;
		success &= sqlMgr.executeStatementGetArray("SELECT object_id, road_user_type, n_objects FROM objects", objects);
		if(success && !objects.empty() && objects[0].size() == 3)
		{
			for (unsigned int row = 0; row <objects.size(); ++row)
			{
				int objectId;
				std::string  objectType;
				success &= Utils::String::StringToType(objects[row][0], objectId);
				objectType = objects[row][1];
				objectToTypeMap[objectId] = objectType;
				otm.addType(objectType);
			}
		}
		updatePercent(60);
		LOGASSERT(success, "Object failed loading. Did you group the objects ?");
		//On charge les association objet/trajectories
		ObjectTrajectoryMap objectList;
		std::vector<std::vector<std::string>> objects_features;
		success &= sqlMgr.executeStatementGetArray("SELECT object_id, trajectory_id FROM objects_features", objects_features);

		if(success && !objects_features.empty() && objects_features[0].size() == 2)
		{
			for (unsigned int row = 0; row <objects_features.size(); ++row)
			{
				int objectId, trajId;
				success &= Utils::String::StringToType(objects_features[row][0], objectId);
				success &= Utils::String::StringToType(objects_features[row][1], trajId);
				trajIdUsedMap[trajId] = true;
				objectList[objectId].push_back(trajectories[trajId]);
				updatePercent(60 + 20*(float)row/objects_features.size());
			}
		}
		LOGASSERT(success, "Object/Trajectories association failed loading");
		std::map<unsigned int, DrawableSceneView*>& timestampToScene = getAppContext()->getTimestampToScene();
		std::vector<SceneObject*>& sceneObjectList = getAppContext()->getSceneObjectList();
		//On charge les objets dans la gui
		int nbPointBoxes = 0;

		int inc = 0;
		for(auto objIt = objectList.begin(); objIt != objectList.end(); ++objIt)
		{			
			int objId = (*objIt).first;			
			const std::vector<PointList>& pointList = (*objIt).second;
			SceneObject* so = new SceneObject(Utils::String::toString(objId), "", objectToTypeMap[objId]);
			//On associe tout les points � un temps
			std::map<int, std::vector<cv::Point>> timestampToPointMap;
			for(auto ptListIt = pointList.begin(); ptListIt != pointList.end(); ++ptListIt)
			{
				const PointList& pl = (*ptListIt);
				for(auto ptIt = pl.begin(); ptIt != pl.end(); ++ptIt)
					timestampToPointMap[(*ptIt).first].push_back((*ptIt).second);
			}

			QColor color(rand()%255,rand()%255, rand()%255);


			//On cr�e les boites englobante correspondante
			for(auto timeIt = timestampToPointMap.begin(); timeIt != timestampToPointMap.end(); ++timeIt)
			{
				std::vector<cv::Point> ptList = (*timeIt).second;
				if(ptList.size() == 1)
					++nbPointBoxes;
	
				if(!ptList.empty())
				{
					cv::Rect bb = getBoundingBox(ptList);
					ObjectObservation* objObs = new ObjectObservation(color, false);
					objObs->setRect(QRectF(bb.x, bb.y, bb.width, bb.height));
					objObs->associateToSceneObject(so);
					int currentTimestamp = (*timeIt).first;
					auto it = timestampToScene.find(currentTimestamp);
					if(it == timestampToScene.end())
					{
						auto itPair = timestampToScene.insert(std::pair<unsigned int, DrawableSceneView*>(currentTimestamp, new DrawableSceneView(currentTimestamp, nullptr, getAppContext())));
						it = itPair.first;
					}

					DrawableSceneView* scene = (*it).second;
					scene->addItem(objObs);
				}
			
			}
			sceneObjectList.push_back(so);
			++inc;
			updatePercent(80 + 20*(float)inc/objectList.size());
		}
		if(nbPointBoxes!=0)
			LOGWARNING(nbPointBoxes << " boxes had only 1 point. They might not be visible.");

		int unusedTraj = 0;
		for(auto mapIt = trajIdUsedMap.begin(); mapIt != trajIdUsedMap.end(); ++mapIt)
		{
			if(mapIt->second == false)
				++unusedTraj;
		}
		if(unusedTraj != 0)
		{
			LOGWARNING(unusedTraj << "/" << trajIdUsedMap.size() << " trajectories are not associated to an object");
		}



	}
	if(!success)
		LOGERROR("Error were generated during importation");

}


cv::Rect TrafficIntelligenceSQLImporter::getBoundingBox(const std::vector<cv::Point>& points)
{
	unsigned int minX = -1, minY = -1;
	unsigned int maxX = 0, maxY = 0;

	for(auto ptIt = points.begin(); ptIt != points.end(); ++ptIt)
	{
		unsigned int x = (*ptIt).x;
		unsigned int y = (*ptIt).y;
		if(x > maxX)
			maxX = x;
		if(y > maxY)
			maxY = y;
		if(x < minX)
			minX = x;
		if(y < minY)
			minY = y;
	}

	int deltaX = maxX-minX;
	int deltaY = maxY-minY;
	

	return cv::Rect(minX, minY, deltaX > 0 ? deltaX: 1, deltaY > 0 ? deltaY: 1);
}