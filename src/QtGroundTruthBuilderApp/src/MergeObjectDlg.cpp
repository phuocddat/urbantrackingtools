#include "MergeObjectDlg.h"
#include "AppContext.h"
#include "SceneObject.h"
#include <QString>
#include "ObjectObservation.h"
#include "DrawableSceneView.h"
MergeObjectDlg::MergeObjectDlg(QWidget *parent, AppContext* appContext)
: QDialog(parent) 
, mContext(appContext)
{
	mUi.setupUi(this);
	mUi.listWidget->setSelectionMode(QAbstractItemView::MultiSelection);
	const std::vector<SceneObject*>& objectList =  mContext->getSceneObjectList();
	for(auto it = objectList.begin(); it != objectList.end(); ++it)
	{
		QListWidgetItem* item = new QListWidgetItem(QString(((*it)->getId()+" "+(*it)->getDescription()).c_str()));
		mItemToObjectMap[item] = *it;
		mUi.listWidget->addItem(item);
	}
}



void MergeObjectDlg::run()
{
	std::vector<SceneObject*> selectedObject;
	auto selectedItemList = mUi.listWidget->selectedItems();
	for(auto itemIt = selectedItemList.begin(); itemIt != selectedItemList.end(); ++itemIt)	
		selectedObject.push_back(mItemToObjectMap[(*itemIt)]);
	

		if(selectedObject.size() > 1)
		{
			std::set<SceneObject*> objectToMerge;
			SceneObject* objectToMergeInto = selectedObject[0];
			for(int i = 1; i < selectedObject.size(); ++i)
				objectToMerge.insert(selectedObject[i]);

			auto& timestampMap = mContext->getTimestampToScene();
			for(auto it = timestampMap.begin(); it != timestampMap.end(); ++it)
			{
				DrawableSceneView* dsv = (*it).second;


				std::vector<ObjectObservation*> objObservationFrame;
				QList<QGraphicsItem *> items = dsv->items();
				for (int i = 0; i < items.size(); ++i)
				{
					ObjectObservation* obj = dynamic_cast<ObjectObservation*>(items[i]);
					if(obj)
					{
						if(objectToMerge.find(obj->getSceneObject()) != objectToMerge.end())			
						{
							obj->associateToSceneObject(objectToMergeInto);			
							objObservationFrame.push_back(obj);
						}
						else if(obj->getSceneObject() == objectToMergeInto)
							objObservationFrame.push_back(obj);
					}

				}
				if(objObservationFrame.size()>1)
				{
					auto obsIt = objObservationFrame.begin();
					QRectF mergedRect = (*obsIt)->rect();
					auto obsIt2 = obsIt;
					++obsIt2;
					
					for(; obsIt2!= objObservationFrame.end(); ++obsIt2)
					{
						mergedRect = mergedRect.united((*obsIt2)->rect());
						(*it).second->removeItem(*obsIt2);
					}
					(*objObservationFrame.begin())->setRect(mergedRect);
				}
			}

			//On enl�ve les objets vide de la DB
			std::vector<SceneObject*>& objectList = mContext->getSceneObjectList();
			for(auto it = objectList.begin(); it != objectList.end(); )
			{
				if(objectToMerge.find(*it) != objectToMerge.end())
					it = objectList.erase(it);
				else
					++it;
			}
		}
}
		

