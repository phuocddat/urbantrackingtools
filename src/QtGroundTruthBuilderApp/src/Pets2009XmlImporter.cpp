#include "Pets2009XmlImporter.h"

#include <QFile>
#include <qdom.h>
#include <QDir>
#include "DrawableSceneView.h"
#include "SceneObject.h"
#include "ObjectObservation.h"
#include <time.h>
#include "ObjectTypeManager.h"
#include "AppContext.h"

Pets2009XmlImporter::Pets2009XmlImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "Pets 2009 XML", "", "PETS2009", GTImporter::FILE, videoWidth, videoHeight)
{

	

}


Pets2009XmlImporter::~Pets2009XmlImporter()
{

}


void Pets2009XmlImporter::LoadDatabase()
{

	std::map<unsigned int, DrawableSceneView*>& sceneMap = getAppContext()->getTimestampToScene();
	QDomDocument doc;
	QFile f(getLoadPath());
	f.open(QIODevice::ReadOnly);
	doc.setContent(&f);
	f.close();
	srand ( time(NULL) ); 
	//We parse the DOM tree
	QDomElement root=doc.documentElement();
	QDomElement child=root.firstChild().toElement();
	std::map<QString, SceneObject*> idToSceneObject;
	while(!child.isNull())
	{
		QString objectId = child.attribute("obj_id", "0");
		QString objectType = child.attribute("obj_type", "Human");
		auto objectIt = idToSceneObject.find(objectId);
		SceneObject* so = nullptr;
		if(objectIt != idToSceneObject.end())
			so = (*objectIt).second;				
		else
		{
			so = new SceneObject(objectId.toStdString(), objectType.toStdString(), objectType.toStdString()); 
			idToSceneObject.insert(std::make_pair(objectId,so));
			getAppContext()->getSceneObjectList().push_back(so);
		}

		QDomElement grandChild=child.firstChild().toElement();
		while(!grandChild.isNull())
		{
			QString timestampStr = grandChild.attribute("frame_no", "0");
			int timestamp = timestampStr.toInt();
			QString xStr = grandChild.attribute("x", "0");
			QString yStr = grandChild.attribute("y", "0");
			QString widthStr = grandChild.attribute("width", "0");
			QString heightStr = grandChild.attribute("height", "0");
			auto it = sceneMap.find(timestamp);
			if(it == sceneMap.end())
			{
				auto itPair = sceneMap.insert(std::pair<unsigned int, DrawableSceneView*>(timestamp, new DrawableSceneView(timestamp, nullptr, getAppContext())));
				it = itPair.first;
			}
			DrawableSceneView* currentScene = it->second;


			ObjectObservation* objObs = new ObjectObservation(QColor(0,0,0), false);
			objObs->associateToSceneObject(so);
			objObs->setRect(QRectF(xStr.toInt(), yStr.toInt(), widthStr.toInt(), heightStr.toInt()));
			currentScene->addItem(objObs);

			grandChild = grandChild.nextSibling().toElement();
		}
		child = child.nextSibling().toElement();	
	}
}

