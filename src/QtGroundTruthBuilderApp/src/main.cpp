#include "QtGroundTruthBuilderApp.h"
#include <QtGui/QApplication>
#include <time.h>
#ifdef WIN32
#include <windows.h>
#endif
#include <iostream>
#include <set>
#include "UrbanTrackerExporter.h"
#include "MatrixIO.h"
#include "GTImporter.h"
#include "AppContext.h"
#include "InputFrameProviderIface.h"
#include "PolyTrackExporter.h"
int main(int argc, char *argv[])
{
#ifdef WIN32
	if(AllocConsole())
	{
		freopen("CONOUT$","wb",stdout);
		freopen("CONOUT$","wb",stderr);
	}
#endif
	QCoreApplication::setOrganizationName("�cole Polytechnique de Montr�al");
	QCoreApplication::setApplicationName("Ground Truth Builder");
	QApplication a(argc, argv);
	QStringList argList = a.arguments();
	std::set<QString> helpString;
	helpString.insert("?");
	helpString.insert("\\?");
	helpString.insert("help");
	helpString.insert("\\help");
	if(argList.size() > 1  && helpString.find(argList[1]) != helpString.end())
	{
		std::cout << "Usage is: " << argList[0].toStdString() << " pathtoVideo inputDbFormat inputDb sqliteName homography\nEx: " 
			<< argList[0].toStdString() << " C:\\video.avi inputDbFormat C:\\input.xml C:\\output.sqlite C:\\homography.txt \n inputDbFormat can be: CAVIAR, TISQL, LUND, PETS2001, TRAFFICINT, UrbanTrackSQL";
	}
	else
	{
		for(unsigned int i = 0; i < argList.size(); ++i)
			std::cout << argList[i].toStdString()  << std::endl;


		QtGroundTruthBuilderApp w;
		srand ( time(NULL) ); 
		if(argList.size() > 4)
		{
			QString videoPath = argList[1];
			w.LoadVideo(videoPath);
			QString type = argList[2];
			QString inputPath = argList[3];			
			QString savePath;
			
			w.seekToFrame(0);
			savePath = argList[4];
			auto it = w.getImporterList().find(type);
			if(it != w.getImporterList().end())
			{
				w.getContext()->clear();
				GTImporter* importer = it->second;
				if(importer->askHomography() && argList.size() > 5)
				{
					cv::Mat homographyMatrix;
					QString homographyPath = argList[5];
					Utils::IO::LoadMatrix<double>(homographyPath.toStdString(), homographyMatrix);
					cv::transpose(homographyMatrix, homographyMatrix);
					cv::invert(homographyMatrix,homographyMatrix);
					importer->setHomography(homographyMatrix);
				}
				importer->setLoadPath(inputPath);
				importer->run();
				PolyTrackExporter exporter(nullptr, w.getContext());
				exporter.setSaveInfo(savePath.toStdString(), w.getVideoInput()->getHeight(), w.getVideoInput()->getWidth());
				exporter.run();	
			}
		}
		if(argList.size() <= 4)
		{		
			w.show();
			a.exec();
		}
	}

	return 0;
}
