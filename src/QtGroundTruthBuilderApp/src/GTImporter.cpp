#include "GTImporter.h"
#include "AppContext.h"
#include <opencv2/opencv.hpp>

GTImporter::GTImporter(StatusProgressBar* progressBarControl, AppContext* appcontext, const QString& name, const QString& extension, const QString& shortName, InputType type, int w, int h)
: BaseTask(progressBarControl, "Loading " + shortName)
, mName(name)
, mExtension(extension)
, mType(type)
, mWidth(w)
, mHeight(h)
, mAppContext(appcontext)
, mShortName(shortName)
, mHomography(cv::Mat::eye(3,3, CV_MAKETYPE(cv::DataDepth<double>::value ,1)))
{

}


void GTImporter::run()
{
	LoadDatabase();
}