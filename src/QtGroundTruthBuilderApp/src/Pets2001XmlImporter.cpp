#include "Pets2001XmlImporter.h"
#include <QFile>
#include <qdom.h>
#include <QDir>
#include "DrawableSceneView.h"
#include "SceneObject.h"
#include "ObjectObservation.h"
#include <time.h>
#include "ObjectTypeManager.h"
#include "AppContext.h"

Pets2001XmlImporter::Pets2001XmlImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl, appContext, "Pets 2001 XML", "", "PETS2001", GTImporter::DIRECTORY, videoWidth, videoHeight)
{

	

}


Pets2001XmlImporter::~Pets2001XmlImporter()
{

}


void Pets2001XmlImporter::LoadDatabase()
{
	QDir dir(getLoadPath());
	QStringList filters;
	filters << "*.xml" << "*.XML";
	QStringList list = dir.entryList(filters);
	updatePercent(10);
	int inc = 0;
	for(auto it = list.begin(); it != list.end(); ++it)
	{
		QString fullAbsPath = dir.absoluteFilePath(*it);
		LoadFile(fullAbsPath);
		++inc;
		updatePercent((inc/list.size())*90+10);		
	}
}

void Pets2001XmlImporter::LoadFile(QString fileName)
{
	QDomDocument doc;
	QFile f(fileName);
	f.open(QIODevice::ReadOnly);
	doc.setContent(&f);
	f.close();
	srand ( time(NULL) ); 
	//We parse the DOM tree
	QDomElement root=doc.documentElement();
	QString objectType = root.attribute("CLASS", "UNKNOWN");
	getAppContext()->getObjectTypeManager().addType(objectType.toStdString());
	QString description = root.attribute("DESCRIPTION", "");
	QString id = root.attribute("ID", "-1");
	QColor color(rand()%255,rand()%255, rand()%255);
	SceneObject* sceneObj = new SceneObject(id.toStdString(), description.toStdString(), objectType.toStdString());
	getAppContext()->getSceneObjectList().push_back(sceneObj);
	// We traverse its children
	QDomElement child=root.firstChild().toElement();
	while(!child.isNull())
	{
		QString timestampStr = child.attribute("TIME", "-1");
		int timestamp = timestampStr.toInt();
		if(timestamp != -1)
		{
			std::map<unsigned int, DrawableSceneView*>& sceneMap = getAppContext()->getTimestampToScene();
			auto it = sceneMap.find(timestamp);
			if(it == sceneMap.end())
			{
				auto itPair = sceneMap.insert(std::pair<unsigned int, DrawableSceneView*>(timestamp, new DrawableSceneView(timestamp, nullptr, getAppContext())));
				it = itPair.first;
			}
			DrawableSceneView* currentScene = it->second;
			QDomElement grandChild=child.firstChild().toElement();
			while(!grandChild.isNull())
			{
				if (grandChild.tagName() == "OBSERVATION")
				{
					QDomElement properties=grandChild.firstChild().toElement();
					while(!properties.isNull())
					{
						if (properties.tagName() == "BOUNDING_BOX")
						{
							int Bottom = properties.attribute("BOTTOM", "-1").toInt();
							int Top = properties.attribute("TOP", "-1").toInt();
							int Left = properties.attribute("LEFT", "-1").toInt();
							int Right = properties.attribute("RIGHT", "-1").toInt();
							ObjectObservation* objObs = new ObjectObservation(color, false);
							objObs->setRect(QRectF(Left, getVideoHeight()-Top, Right-Left, Top-Bottom));
							objObs->associateToSceneObject(sceneObj);
							currentScene->addItem(objObs);
						}
						properties = properties.nextSibling().toElement();
					}
				}
				grandChild = grandChild.nextSibling().toElement();
			}


		}

		child = child.nextSibling().toElement();
	}
}
