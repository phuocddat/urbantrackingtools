#include "InterpolationManager.h"
#include "AppContext.h"
#include "SceneObject.h"
#include "DrawableSceneView.h"
#include "ObjectObservation.h"
#include <limits>

void Utils::interpolate(AppContext* context, SceneObject* objectToInterpolate, int startFrame, int endFrame, bool allTimestamp)
{
	if(startFrame < endFrame || allTimestamp)
	{
		

		auto& timestampToSceneMap = context->getTimestampToScene();
		if(allTimestamp)
		{
			startFrame = std::numeric_limits<int>::max();
			endFrame = 0;

			for(auto it = timestampToSceneMap.begin(); it != timestampToSceneMap.end(); ++it)
			{
				
				int timestamp = (*it).first;
				QList<QGraphicsItem *> items = (*it).second->items();
				for (int i = 0; i < items.size(); ++i)
				{
					ObjectObservation* obs = dynamic_cast<ObjectObservation*>(items[i]);
					if(obs && obs->getSceneObject() == objectToInterpolate)
					{
						if(timestamp > endFrame)
							endFrame = timestamp;
						if(timestamp < startFrame)
							startFrame = timestamp;						
					}
				}

			}
		}

		cv::Rect first;
		cv::Rect last;

		std::vector<std::pair<ObjectObservation*, int>> interpolatedObservation;
		std::vector<std::pair<ObjectObservation*, int>> nonInterpolatedObservation;
		for(unsigned int t = startFrame; t <= endFrame; ++t)
		{
			DrawableSceneView* dsv = nullptr;
			auto sceneIt = timestampToSceneMap.find(t);
			if(sceneIt == timestampToSceneMap.end())
			{
				dsv = new DrawableSceneView(t, nullptr, context);
				timestampToSceneMap.insert(std::make_pair(t,dsv));
			}
			else 
				dsv = sceneIt->second;

			QList<QGraphicsItem *> items = dsv->items();
			for (int i = 0; i < items.size(); ++i)
			{
				ObjectObservation* obs = dynamic_cast<ObjectObservation*>(items[i]);
				if(obs && obs->getSceneObject() == objectToInterpolate)
				{
					if(obs->isInterpolated())				
						interpolatedObservation.push_back(std::make_pair(obs,t));				
					else
						nonInterpolatedObservation.push_back(std::make_pair(obs,t)); 
				}
			}
		}

		//On enl�ve les anciennes interpolation
		for(unsigned int i = 0; i < interpolatedObservation.size(); ++i)	
			timestampToSceneMap[interpolatedObservation[i].second]->removeItem(interpolatedObservation[i].first);

		QColor color(objectToInterpolate->getColor().R, objectToInterpolate->getColor().G,objectToInterpolate->getColor().B);
		if(!nonInterpolatedObservation.empty())
		{
			for(unsigned int i = 0; i < nonInterpolatedObservation.size() - 1; ++i)
		{
			int startTimestamp = nonInterpolatedObservation[i].second;
			int endTimestamp = nonInterpolatedObservation[i+1].second;
			QRectF startObs = nonInterpolatedObservation[i].first->rect();
			QRectF endObs = nonInterpolatedObservation[i+1].first->rect();
			for(unsigned int t = startTimestamp+1; t < endTimestamp; ++t)
			{
				int timeLapse = endTimestamp-startTimestamp;
				float ratio = ((float)t-startTimestamp)/timeLapse;
				float ratioStart = 1-ratio;
				float ratioEnd = ratio;

				int x = ratioStart*startObs.x()+ratioEnd*endObs.x();
				int y = ratioStart*startObs.y()+ratioEnd*endObs.y();
				int w = ratioStart*startObs.width()+ratioEnd*endObs.width();
				int h = ratioStart*startObs.height()+ratioEnd*endObs.height();

				ObjectObservation* objObs = new ObjectObservation(color, true);
				objObs->associateToSceneObject(objectToInterpolate);
				objObs->setRect(x,y,w,h);
				auto sceneIt = timestampToSceneMap.find(t);
				if(sceneIt != timestampToSceneMap.end())			
					(*sceneIt).second->addItem(objObs);
				else
					std::cout << "Error scene not found " << t;
			}
		}
		}
		
	}	
}