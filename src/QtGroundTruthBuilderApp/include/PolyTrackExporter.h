#ifndef POLY_TRACK_EXPORTER_H
#define POLY_TRACK_EXPORTER_H

#include "ExporterTask.h"
class StatusProgressBar;
class SQLiteManager;

class PolyTrackExporter: public ExporterTask
{
	Q_OBJECT
public:
	PolyTrackExporter(StatusProgressBar* progressBarControl, AppContext* context);
	virtual ~PolyTrackExporter();


	void run();
private:
	bool createTables();
	bool dropTables();
	bool saveObjectTypeTable();
	bool saveBoundingBox();
	bool saveSceneObjectTable();
	
	bool saveCurrentContext();


	SQLiteManager* mSQLManager;

};

#endif