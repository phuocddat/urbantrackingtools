#ifndef BOUNDING_BOX
#define BOUNDING_BOX

#include <opencv2/opencv.hpp>
class BoundingBoxHandler;
class BoundingBox
{
public:
	BoundingBox(unsigned int x0, unsigned int x1, unsigned int y0, unsigned int y1, cv::Scalar color, unsigned int thickness = 2);
	virtual ~BoundingBox();
	
	void draw(cv::Mat& background);
	void draw(cv::Mat& background, cv::Scalar color, unsigned int thickness);
	void setSelected(bool selected) { mSelected = selected;}
	BoundingBoxHandler* getHandler() { return mHandler;}
	unsigned int getStartX() { return mX0; }
	unsigned int getStartY() { return mY0; }
	unsigned int getEndX() { return mX1; }
	unsigned int getEndY() { return mY1; }
	void setStartX(unsigned int startX) 
	{ 
		if(startX > mX1)
			mX1 = mX0;
		mX0 = startX; 
	}
	void setStartY(unsigned int startY) 
	{  
		if(startY > mY1)
			mY1 = mY0;
		mY0 = startY; 
	}
	void setEndX(unsigned int endX) 
	{  
		if(endX < mX0)
			mX0 = mX1;
		mX1 = endX; 
	}
	void setEndY(unsigned int endY) 
	{ 
		if(endY < mY0)
			mY0 = mY1;
		mY1 = endY; 
	}
private:

	unsigned int mThickness;
	cv::Scalar mColor;

	unsigned int mX0;
	unsigned int mY0;
	unsigned int mX1;
	unsigned int mY1;
	bool mSelected;
	BoundingBoxHandler* mHandler;
};


#endif