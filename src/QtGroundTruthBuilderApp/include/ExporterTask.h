#ifndef EXPORTER_TASK_H
#define EXPORTER_TASK_H

#include "BaseTask.h"
#include <string>

class AppContext;
class StatusProgressBar;

class ExporterTask : public BaseTask
{
	Q_OBJECT
public:
	ExporterTask(StatusProgressBar* progressBarControl, AppContext* context, const QString& name, const QString& extension);
	virtual ~ExporterTask();
	virtual void run() = 0;
	void cleanObjectsId();
	const QString& getName() const {return mName;}
	void setSaveInfo(const std::string& path, int videoHeight, int videoWidth) { mFilePath = path; mWidth = videoWidth; mHeight = videoHeight;}
	bool isOwnedByThread() {return false;}
	const QString& getExtension() const {return mExtension;}

protected:
	AppContext* mContext;
	QString mName;
	std::string mFilePath;
	int mWidth;
	int mHeight;
	QString mExtension;

};

#endif