#ifndef NEGATIVE_SAMPLE_DLG
#define NEGATIVE_SAMPLE_DLG

#include <qdialog.h>
#include "ui_ExtractGTDlg.h"

class NegativeSampleDlg: public QDialog
{
		Q_OBJECT
public:
		NegativeSampleDlg(QWidget *parent=0);
		void getValues(int& minX, int& maxX, int& minY, int& maxY, int& nbSamples);

signals:
		void LaunchExtraction(int minX, int maxX, int minY, int maxY, int nbSamples);

private:
	Ui::ExtractNegativeSampleDlg mUi;
};


#endif