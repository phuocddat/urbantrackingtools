#ifndef INTERPOLATION_MANAGER_H
#define INTERPOLATION_MANAGER_H

class AppContext;
class SceneObject;

namespace Utils
{
	void interpolate(AppContext* context, SceneObject* so, int startFrame, int endFrame, bool allTimestamp);
}



#endif