#ifndef PROGRESS_DIALOG_H
#define PROGRESS_DIALOG_H

#include "qwidget.h"
#include "qdialog.h"
#include "BaseTask.h"
class StatusProgressBar;


class ProgressDialog : public QDialog
{
public:
	ProgressDialog(StatusProgressBar* progressBar, QWidget* parent);
	StatusProgressBar* getStatusBarProgressControl() const { return mStatusBarProgressControl;}
	virtual BaseTask* getBaseTask()  = 0;
private:
	StatusProgressBar* mStatusBarProgressControl;

};


#endif