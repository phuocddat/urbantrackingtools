#ifndef BOUNDING_BOX_HANDLER_H
#define BOUNDING_BOX_HANDLER_H

#include <opencv2/opencv.hpp>


class BoundingBox;

enum HandlerControl
{
	NONE = 0,
	TOPLEFTCORNER,
	TOPRIGHTCORNER,
	BOTTOMLEFTCORNER,
	BOTTOMRIGHTCORNER,
	LEFTSCALE,
	RIGHTSCALE,
	TOPSCALE,
	BOTTOMSCALE,
	TRANSLATION
};

class BoundingBoxHandler
{
public:
	BoundingBoxHandler(BoundingBox* boundingBox);
	~BoundingBoxHandler();

	void draw(cv::Mat& img);
	




	HandlerControl getSelectedHandler(cv::Mat& background, unsigned int x, unsigned int y);

private:
	void draw(cv::Mat& img, bool picking);
	enum ArrowType
	{
		LEFTARROW,
		RIGHTARROW,
		UPARROW,
		DOWNARROW
	};

	void drawArrow(cv::Mat& background, cv::Point startPt, cv::Point endPt, cv::Scalar color, BoundingBoxHandler::ArrowType type);
	BoundingBox* mBoundingBox;
};

#endif