#ifndef URBAN_TRACKER_EXPORTER_H
#define URBAN_TRACKER_EXPORTER_H

#include "ExporterTask.h"
class SQLiteManager;

class UrbanTrackerExporter: public ExporterTask
{
public:
	UrbanTrackerExporter(StatusProgressBar* progressBarControl, AppContext* context);
	virtual ~UrbanTrackerExporter();

	void run();
private:
	bool createTables();
	bool dropTables();
	bool saveObjectTypeTable();
	bool savePositionTable();
	bool saveSceneObjectTable();
	
	bool saveCurrentContext();


	SQLiteManager* mSQLManager;
};


#endif