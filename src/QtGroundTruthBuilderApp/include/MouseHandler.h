#ifndef MOUSE_HANDLER_H
#define MOUSE_HANDLER_H

#include <opencv.hpp>

class MouseHandler
{
public:
	virtual ~MouseHandler(){};

	virtual void onMouseDown(int x, int y) = 0;
	virtual void onMouseMove(int x, int y) = 0;
	virtual void onMouseUp(int x, int y) = 0;
	virtual void  draw(cv::Mat& img) {};
};



#endif