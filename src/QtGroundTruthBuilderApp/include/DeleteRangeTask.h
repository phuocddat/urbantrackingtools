#ifndef DELETE_RANGE_TASK
#define DELETE_RANGE_TASK

#include "BaseTask.h"
#include <qstring.h>
#include <map>
class InputFrameProviderIface;
class AppContext;
class QListWidgetItem;
class SceneObject;
#include <set>


class DeleteRangeTask : public BaseTask
{
	 Q_OBJECT
 
public:
    DeleteRangeTask(StatusProgressBar* progressBarControl, AppContext* appContext, std::map<QListWidgetItem*, SceneObject*> itemToObjectMap,  std::set<SceneObject*> toDeleteSet, int startFrame, int endFrame, bool allTimestamp);
	~DeleteRangeTask() {}
	bool isOwnedByThread() {return true;}

private:
	virtual void run();
	std::map<QListWidgetItem*, SceneObject*> mItemToObjectMap;
	AppContext* mContext;
	std::set<SceneObject*> mToDeleteSet;
	 int mStartFrame;
 int mEndFrame;
 bool mAllTimestamp;

};



#endif