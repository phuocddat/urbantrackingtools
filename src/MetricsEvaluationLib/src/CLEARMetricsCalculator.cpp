#include "CLEARMetricsCalculator.h"
#include "CLEARDataAssociator.h"
#include <iostream>
#include "Dataset.h"

CLEARMetricsCalculator::CLEARMetricsCalculator(const Dataset* groundTruth, const Dataset* experimentalPath, double threshold, MeasureDistType distType)
: mMappingThreshold(threshold)
, mDataAssociator(nullptr)
, mDistType(distType)
{
	if(groundTruth->isLoaded() && experimentalPath->isLoaded())
	{
		mDataAssociator = new CLEARDataAssociator(groundTruth, experimentalPath, mMappingThreshold, mDistType);
		mMetrics = CLEARMetrics(getMOTP(), getMOTA(), getMissesRatio(), getFPRatio(), getMismatchesRatio(), mDistType);
	}

}
CLEARMetricsCalculator::~CLEARMetricsCalculator()
{

}

double CLEARMetricsCalculator::getMOTP()
{
	double motp = -1;
	if(mDataAssociator)
	{
		double dist = 0;
		auto& dit = mDataAssociator->getDIt();
		for(unsigned int i = 0; i < dit.size(); ++i)
		{
			for(unsigned int j = 0; j < dit[i].size(); ++j)
			{
				if(mDistType == ABSOLUTEDIST)
					dist += dit[i][j];
				else if(mDistType == BOUNDINGBOXOVERLAP)
					dist += 1-dit[i][j];
				else
					std::cout << "Erreur, type de distance inconnu" << std::endl;
			}			
		}

		double c = 0;
		auto& ct = mDataAssociator->getCt();
		for(unsigned int i = 0; i < ct.size(); ++i)
		{
			c+= ct[i];
		}
		
		motp = c > 0 ? (dist)/c : 0;

		
	}
	return motp;
}
double CLEARMetricsCalculator::getMOTA()
{
	double mota = -1;
	if(mDataAssociator)
	{
		auto& mt = mDataAssociator->getMt();
		auto& fpt = mDataAssociator->getFPt();
		auto& mmet = mDataAssociator->getMMEt();
		auto& gt = mDataAssociator->getGt();

		int m = 0, fp =0, mme = 0, g = 0;
		for(unsigned int i = 0; i < mt.size(); ++i)
		{
			m += mt[i];
			fp += fpt[i];
			mme += mmet[i];
			g += gt[i];
		}
		
		mota = g > 0 ? 1 - (double)(m+fp+mme)/(double)(g) : 0;

	}
	return mota;
}
double CLEARMetricsCalculator::getMissesRatio()
{
	double missesRatio = -1;
	if(mDataAssociator)
	{
		auto& mt = mDataAssociator->getMt();
		auto& gt = mDataAssociator->getGt();

		int m = 0, g = 0;
		for(unsigned int i = 0; i < mt.size(); ++i)
		{
			m += mt[i];
			g += gt[i];
		}

		missesRatio = g > 0 ?(double)(m)/(double)(g) :0;

	}
	return missesRatio;
}
double CLEARMetricsCalculator::getFPRatio()
{
	double FPRatio = -1;
	if(mDataAssociator)
	{
		auto& fpt = mDataAssociator->getFPt();
		auto& gt = mDataAssociator->getGt();

		int fp = 0, g = 0;
		for(unsigned int i = 0; i < gt.size(); ++i)
		{
			fp += fpt[i];
			g += gt[i];
		}

		FPRatio = g > 0 ? (double)(fp)/(double)(g):0;

	}
	return FPRatio;
}
double CLEARMetricsCalculator::getMismatchesRatio()
{
	double mismatchesRatio = -1;
	if(mDataAssociator)
	{
		auto& mmet = mDataAssociator->getMMEt();
		auto& gt = mDataAssociator->getGt();

		int mme = 0, g = 0;
		for(unsigned int i = 0; i < gt.size(); ++i)
		{
			mme += mmet[i];
			g += gt[i];
		}
		
		mismatchesRatio = g > 0 ?(double)(mme)/(double)(g) :0;
	}
	return mismatchesRatio;
}

