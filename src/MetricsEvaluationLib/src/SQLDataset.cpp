#include "SQLDataset.h"
#include "Observation.h"
#include "SQLiteManager.h"
#include "SceneObject.h"
#include <string>
#include <iostream>
#include "Logger.h"

SQLDataset::SQLDataset()
: mSQL(nullptr)
, mHomography(cv::Mat::eye(3,3,CV_32FC1))
{
	
}

SQLDataset::~SQLDataset()
{

	delete mSQL;
}


bool SQLDataset::Load(const std::string& databaseFile)
{
	mObjectIdToObjectMap.clear();
	bool success = true;
	mSQL = new SQLiteManager(databaseFile);
	if(mSQL->isConnected())
	{
		bool objectTypeTableDefined = mSQL->tableExist("objects_type");
		bool objectsTableDefined = mSQL->tableExist("objects");
		bool boundingBoxTableDefined = mSQL->tableExist("bounding_boxes");
		bool objectsFeaturesTableDefined = mSQL->tableExist("objects_features");
		bool positionsTableDefined = mSQL->tableExist("positions");

		if((boundingBoxTableDefined || (positionsTableDefined && objectsFeaturesTableDefined)) && objectsTableDefined)
		{
			if(objectTypeTableDefined)
				success &= LoadObjectTypeTable();
			
			success &= LoadObjectsTable();

			if(boundingBoxTableDefined)			
				success &= LoadBoundingBoxesTable();			
			else			
				success &= LoadPointsTable();					
			

		}
		else
		{
			LOGERROR("Invalid table combination is unsupported.");
			return success;
		}
	}
	if(!success)
		LOGERROR("Error were generated during importation");
	mObjectIdToObjectMap.clear();
	mLoaded = success;
	return success;
}

bool SQLDataset::LoadObjectTypeTable()
{
	std::vector<std::vector<std::string>> objects;
	bool success = mSQL->executeStatementGetArray("SELECT road_user_type, type_string FROM objects_type", objects);
	if(success && !objects.empty() && objects[0].size() == 2)
	{
		for (unsigned int row = 0; row <objects.size(); ++row)
		{
			int typeKey;
			std::string  objectType;
			success &= Utils::String::StringToType(objects[row][0], typeKey);
			objectType = objects[row][1];
			mTypeManager.addType(typeKey, objectType);
		}
	}
	return success;
}

bool SQLDataset::LoadObjectsTable()
{
	std::vector<std::vector<std::string>> objects;
	bool descriptionExist = mSQL->tableFieldExist("objects", "description");
	std::string query = descriptionExist ? "SELECT object_id, road_user_type, description FROM objects" : "SELECT object_id, road_user_type FROM objects";
	std::vector<SceneObjectWithObservation*>& sceneObjectList = mSceneObject;
	bool success = mSQL->executeStatementGetArray(query, objects);
	if(success && !objects.empty() && (objects[0].size() == 2 || objects[0].size() == 3))
	{
		for (unsigned int row = 0; row <objects.size(); ++row)
		{

			int roadUserType;
			success &= Utils::String::StringToType(objects[row][1], roadUserType);
			std::string description(descriptionExist ? objects[row][2] : "");
			if(roadUserType == -1) //TrafficIntelligence -1 is our 0 'Undefined'
				roadUserType = 0;
			const std::string& typeName = mTypeManager.getName(roadUserType);
			if(mTypeManager.getIdx(typeName) == -1)
				mTypeManager.addType(typeName);

			SceneObject* so = new SceneObject(objects[row][0], description, typeName);
			SceneObjectWithObservation* objWithObs = new SceneObjectWithObservation();
			objWithObs->mSceneObject = so;

			sceneObjectList.push_back(objWithObs);	


			mObjectIdToObjectMap.insert(std::pair<std::string, SceneObjectWithObservation*>(objects[row][0], objWithObs));
		}
	}
	return success;
}

bool SQLDataset::LoadBoundingBoxesTable()
{
	std::vector<std::vector<std::string>> objectObs;
	bool success = mSQL->executeStatementGetArray("SELECT object_id, frame_number, x_top_left, y_top_left, x_bottom_right, y_bottom_right FROM bounding_boxes", objectObs);
	if(success && !objectObs.empty() && objectObs[0].size() == 6)
	{
		for (unsigned int row = 0; row <objectObs.size(); ++row)
		{
			const std::string& object_id = objectObs[row][0];
			unsigned int frame_number;
			double top_left_corner_x, top_left_corner_y, bottom_right_corner_x, bottom_right_corner_y;
			success &= Utils::String::StringToType(objectObs[row][1], frame_number);
			success &= Utils::String::StringToType(objectObs[row][2], top_left_corner_x);
			success &= Utils::String::StringToType(objectObs[row][3], top_left_corner_y);
			success &= Utils::String::StringToType(objectObs[row][4], bottom_right_corner_x);
			success &= Utils::String::StringToType(objectObs[row][5], bottom_right_corner_y);
			
			Observation* objObs = new Observation(frame_number, Rect(top_left_corner_x, top_left_corner_y, bottom_right_corner_x-top_left_corner_x, bottom_right_corner_y-top_left_corner_y), mObjectIdToObjectMap[object_id]);
			mObjectIdToObjectMap[object_id]->mObservations.push_back(objObs);
			mTimestampToObservation[frame_number].push_back(objObs);			
		}
	}
	return success;
}


bool SQLDataset::LoadPointsTable()
{
	typedef std::map<int, cv::Point> TimestampToPoint; 
	typedef std::map<std::string, TimestampToPoint> TrajIdToPointList;

	TrajIdToPointList trajectories;

	std::vector<std::vector<std::string>> positions;
	bool success = mSQL->executeStatementGetArray("SELECT trajectory_id, frame_number, x_coordinate, y_coordinate FROM positions", positions);
	
	if(success && !positions.empty() && positions[0].size() == 4)
	{
		for (unsigned int row = 0; row <positions.size(); ++row)
		{
			
			int frame_number;
			double xPos, yPos;
			std::string trajId = positions[row][0];

			success &= Utils::String::StringToType(positions[row][1], frame_number);
			success &= Utils::String::StringToType(positions[row][2], xPos);
			success &= Utils::String::StringToType(positions[row][3], yPos);
			TimestampToPoint& pl = trajectories[trajId];

			cv::Point p(xPos, yPos);
			if(!mHomography.empty())
			{

				std::vector<cv::Point2f> beforeHomography;
				beforeHomography.push_back(cv::Point2f(xPos, yPos));

				std::vector<cv::Point2f> afterHomography;
				cv::perspectiveTransform(beforeHomography,afterHomography, mHomography);

				pl[frame_number] = afterHomography[0];
			}
			else
				pl[frame_number] = p;			
		}
	}

	std::map<std::string, std::vector<TimestampToPoint>> ObjectIdtoPointList;




	//Load TrajectoryId/ObjectId association
	std::vector<std::vector<std::string>> objects_features;
	success &= mSQL->executeStatementGetArray("SELECT object_id, trajectory_id FROM objects_features", objects_features);

	if(success && !objects_features.empty() && objects_features[0].size() == 2)
	{
		for (unsigned int row = 0; row <objects_features.size(); ++row)
		{
			std::string objectId = objects_features[row][0];
			std::string trajId = objects_features[row][1];				
			ObjectIdtoPointList[objectId].push_back(trajectories[trajId]);
		}
	}


	int inc = 0;
	for(auto it = ObjectIdtoPointList.begin(); it != ObjectIdtoPointList.end(); ++it)
	{
		SceneObjectWithObservation* so = mObjectIdToObjectMap[it->first];
		
		std::map<int, std::vector<cv::Point>> timestampToPointMap;
		const std::vector<TimestampToPoint>& pointList = it->second;
		for(auto ptListIt = pointList.begin(); ptListIt != pointList.end(); ++ptListIt)
		{
			const TimestampToPoint& pl = (*ptListIt);
			for(auto ptIt = pl.begin(); ptIt != pl.end(); ++ptIt)
				timestampToPointMap[(*ptIt).first].push_back((*ptIt).second);
		}

		for(auto timeIt = timestampToPointMap.begin(); timeIt != timestampToPointMap.end(); ++timeIt)
		{
			std::vector<cv::Point> ptList = (*timeIt).second;
	

			if(!ptList.empty())
			{
				
				cv::Rect bb = getBoundingBox(ptList);
				int currentTimestamp = (*timeIt).first;
				Observation* objObs = new Observation(currentTimestamp, Rect(bb.x, bb.y, bb.width, bb.height), so);
				so->mObservations.push_back(objObs);
				mTimestampToObservation[currentTimestamp].push_back(objObs);
			}
		}

		++inc;
	}
	return success;
}



cv::Rect SQLDataset::getBoundingBox(const std::vector<cv::Point>& pointList)
{
	unsigned int minX = -1, minY = -1;
	unsigned int maxX = 0, maxY = 0;

	for(auto ptIt = pointList.begin(); ptIt != pointList.end(); ++ptIt)
	{
		unsigned int x = (*ptIt).x;
		unsigned int y = (*ptIt).y;

		if(x > maxX)
			maxX = x;
		if(y > maxY)
			maxY = y;
		if(x < minX)
			minX = x;
		if(y < minY)
			minY = y;
	}

	int deltaX = maxX-minX;
	int deltaY = maxY-minY;
	

	return cv::Rect(minX, minY, deltaX > 0 ? deltaX: 1, deltaY > 0 ? deltaY: 1);
}