﻿#include "CLEARDataAssociator.h"
#include "Observation.h"
#include <map>
#include "matrix.h"
#include "munkres.h"
#include "Dataset.h"
#include "MetricsHelpers.h"

CLEARDataAssociator::CLEARDataAssociator(const Dataset* GTPath, const Dataset* experimentalPath, double mappingThreshold, MeasureDistType distType)
: mMappingThreshold(mappingThreshold)
, mDistType(distType)
{
	if(distType == BOUNDINGBOXOVERLAP)
		mMappingThreshold = 1 - mappingThreshold;

	associate(GTPath, experimentalPath);



}


CLEARDataAssociator::~CLEARDataAssociator()
{

}

void CLEARDataAssociator::getMinMaxTimestamp(const std::map<unsigned int, std::vector<Observation*>>& timestampToObservationMap, unsigned int& min, unsigned int& max)
{
	unsigned int tmpMax = 0;
	unsigned int tmpMin = -1;
	for(auto it = timestampToObservationMap.begin(); it != timestampToObservationMap.end(); ++it)
	{
		if((*it).first > tmpMax)
			tmpMax = (*it).first;
		if((*it).first < tmpMin)
			tmpMin = (*it).first;
	}
	min = tmpMin;
	max = tmpMax;
}

void CLEARDataAssociator::associate(const Dataset* groundtruthData, const Dataset* systemData)
{
	const std::vector<SceneObjectWithObservation*>* gtObjects = &groundtruthData->getSceneObjectList();
	const std::vector<SceneObjectWithObservation*>* systemObjects = &systemData->getSceneObjectList();
	//On fait une map<timestamp, observation> et <observation,SceneObject>

	const std::map<unsigned int, std::vector<Observation*>>& groundtruthTimestampToObservationMap = groundtruthData->getTimestampToObservation();
	const std::map<unsigned int, std::vector<Observation*>>& systemTimestampToObservationMap = systemData->getTimestampToObservation();


	unsigned int sysMin, sysMax, gtMin, gtMax;
	getMinMaxTimestamp(groundtruthTimestampToObservationMap, gtMin, gtMax);
	getMinMaxTimestamp(systemTimestampToObservationMap, sysMin, sysMax);
	unsigned startTimestamp = std::min(gtMin, sysMin);
	unsigned endTimestamp = std::max(gtMax, sysMax);


	/*1) For every mapping (oi, hj) in Mt−1, verify if it is still
	valid. If object oi is still visible and tracker hypothesis
	hj still exists at time t, and if their distance does
	not exceed the threshold T,make the correspondence
	between oi and hj for frame t.*/

	std::vector<std::pair<Observation*,Observation*>> lastFrameGtSystemObservationPair;
	for(unsigned int t = startTimestamp; t <= endTimestamp; ++t)
	{		

		std::vector<std::pair<Observation*,Observation*>> currentGtSystemObservationPair;
		auto itGT = groundtruthTimestampToObservationMap.find(t);
		auto itSys = systemTimestampToObservationMap.find(t);

		const std::vector<Observation*>& gtObservationList = itGT != groundtruthTimestampToObservationMap.end() ? (*itGT).second : std::vector<Observation*>();
		const std::vector<Observation*>& sysObservationList = itSys != systemTimestampToObservationMap.end() ?  (*itSys).second: std::vector<Observation*>();

		//On vérifie si les associations précédente existe toujours
		for(auto associationIt = lastFrameGtSystemObservationPair.begin(); associationIt != lastFrameGtSystemObservationPair.end(); ++associationIt)
		{		
			Observation* lastFrameGtObs = (*associationIt).first;
			Observation* lastFrameSysObs = (*associationIt).second;

			Observation* currentGtObs = getObservationOfObject(gtObservationList, lastFrameGtObs->mObject->mSceneObject);
			Observation* currentSysObs = getObservationOfObject(sysObservationList, lastFrameSysObs->mObject->mSceneObject);
			if(currentGtObs && currentSysObs && MetricsHelpers::calculateDistance(currentGtObs,currentSysObs, mDistType) < mMappingThreshold)
			{
				currentGtSystemObservationPair.push_back(std::make_pair(currentGtObs, currentSysObs));
			}
		}
		Matrix<double> scoreMatrix(gtObservationList.size(), sysObservationList.size());
		unsigned int gtIdx = 0;
		for(auto gtIt = gtObservationList.begin(); gtIt != gtObservationList.end(); ++gtIt)
		{
			unsigned int sysIdx = 0;
			for(auto sysIt = sysObservationList.begin(); sysIt != sysObservationList.end(); ++sysIt)
			{
				//
				double dist = MetricsHelpers::calculateDistance(*sysIt,*gtIt, mDistType);
				scoreMatrix(gtIdx, sysIdx) = dist;
				++sysIdx;
			}
			++gtIdx;
		}

		
		int mismatchErrorT = 0;

		std::vector<std::pair<Observation*, Observation*>>	newAssociation = GreedyAssociation(scoreMatrix, gtObservationList, sysObservationList);

		std::vector<std::pair<Observation*, Observation*>>	keptAssociation;
		for(auto assIt = newAssociation.begin(); assIt != newAssociation.end(); ++assIt)
		{
			Observation* gt((*assIt).first);
			Observation* sys((*assIt).second);
			//gtObservationList.erase(gt);
			//sysObservationList.erase(sys);

			bool alreadyExist = false;
			for(auto currentObsIt = currentGtSystemObservationPair.begin(); currentObsIt != currentGtSystemObservationPair.end(); )
			{
				if((*currentObsIt).first == gt && (*currentObsIt).second == sys)
				{
					alreadyExist = true;
					++currentObsIt;
				}
				else if((*currentObsIt).first == gt || (*currentObsIt).second == sys)
				{
					currentObsIt = currentGtSystemObservationPair.erase(currentObsIt);
					++mismatchErrorT;
				}
				else
					++currentObsIt;
			}
			if(!alreadyExist)
				keptAssociation.push_back((*assIt));
		}


		currentGtSystemObservationPair.insert(currentGtSystemObservationPair.end(),keptAssociation.begin(), keptAssociation.end());

		mNumberMismatchesByTime.push_back(mismatchErrorT);
		mNumberMatchesByTime.push_back(currentGtSystemObservationPair.size());



		std::vector<double> matchesDistance;
		for(auto assIt = currentGtSystemObservationPair.begin(); assIt != currentGtSystemObservationPair.end(); ++assIt)
		{
		
			double dist = MetricsHelpers::calculateDistance((*assIt).first, (*assIt).second, mDistType);
			matchesDistance.push_back(dist);
		}
		mDistanceByMatchesByTime.push_back(matchesDistance);

		int nbFP = sysObservationList.size() - currentGtSystemObservationPair.size();
		int nbObjectMissed = gtObservationList.size()-currentGtSystemObservationPair.size();

		if(nbFP < 0)
			nbFP = 0;
		if(nbObjectMissed < 0)
			nbObjectMissed = 0;


		mNumberOfFPByTime.push_back(nbFP);
		mNumberOfMissedByTime.push_back(nbObjectMissed);
		mNumberObjectAtTimeT.push_back(gtObservationList.size());


		lastFrameGtSystemObservationPair = currentGtSystemObservationPair;

	}
}

Observation* CLEARDataAssociator::getObservationOfObject(std::vector<Observation*> observationSet, SceneObject* sceneObject)
{
	Observation* foundObs = nullptr;
	for(auto it = observationSet.begin(); it != observationSet.end() && foundObs == nullptr; ++it)
	{
		if((*it)->mObject->mSceneObject == sceneObject)
			foundObs = *it;
	}
	return foundObs;
}


std::vector<std::pair<Observation*, Observation*>>	CLEARDataAssociator::GreedyAssociation(Matrix<double>& scoreMatrix, std::vector<Observation*> gtObsListIdx, std::vector<Observation*> sysObsListIdx)
{
	std::vector<std::pair<Observation*, Observation*>> resultingMatches;

	Matrix<double> scoreMatrixDist = scoreMatrix;

	Munkres mkres;
	mkres.solve(scoreMatrix);

	for(unsigned int gtIdx = 0; gtIdx < gtObsListIdx.size(); ++gtIdx)
	{
		for(unsigned int sysIdx = 0; sysIdx < sysObsListIdx.size(); ++sysIdx)
		{
			if(scoreMatrix(gtIdx, sysIdx) == 0 && scoreMatrixDist(gtIdx, sysIdx) < mMappingThreshold)
			{
				resultingMatches.push_back(std::make_pair(gtObsListIdx[gtIdx], sysObsListIdx[sysIdx]));
			}
		}
	}

	return resultingMatches;
}
