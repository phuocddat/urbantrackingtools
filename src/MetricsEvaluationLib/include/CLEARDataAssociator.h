#ifndef CLEAR_DATA_ASSOCIATOR_H
#define CLEAR_DATA_ASSOCIATOR_H

#include <vector>
#include <map>
#include <set>
#include "matrix.h"
#include "MeasureDistType.h"
struct SceneObjectWithObservation;
struct  Observation;
class SceneObject;
class Dataset;
class CLEARDataAssociator
{
public:
	CLEARDataAssociator(const Dataset* GTPath, const Dataset* experimentalPath, double mappingThreshold, MeasureDistType distType);
	~CLEARDataAssociator();

	const std::vector<int>& getCt() const {return mNumberMatchesByTime;}
	const std::vector<std::vector<double>>& getDIt() const{ return mDistanceByMatchesByTime;}
	const std::vector<int>& getMt() const { return mNumberOfMissedByTime;}
	const std::vector<int>& getFPt() const { return mNumberOfFPByTime;}
	const std::vector<int>& getMMEt() const { return mNumberMismatchesByTime;}
	const std::vector<int>& getGt() const { return mNumberObjectAtTimeT;}
private:
	std::vector<std::pair<Observation*, Observation*>>	GreedyAssociation(Matrix<double>& scoreMatrix, std::vector<Observation*> gtObsListIdx, std::vector<Observation*> sysObsListIdx);
	void getMinMaxTimestamp(const std::map<unsigned int, std::vector<Observation*>>& timestampToObservationMap, unsigned int& min, unsigned int& max);
	void associate(const Dataset* GTPath, const Dataset* experimentalPath);
	
	Observation* getObservationOfObject(std::vector<Observation*> observationSet, SceneObject* sceneObject);
	double mMappingThreshold;


	std::vector<int> mNumberMatchesByTime; //cT
	std::vector<std::vector<double>> mDistanceByMatchesByTime; // diT
	std::vector<int> mNumberOfMissedByTime; //mT
	std::vector<int> mNumberOfFPByTime; //fpt
	std::vector<int> mNumberMismatchesByTime; // mmeT
	std::vector<int> mNumberObjectAtTimeT; //gT
	MeasureDistType mDistType;

};



#endif