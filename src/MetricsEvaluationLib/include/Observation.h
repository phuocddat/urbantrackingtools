#ifndef OBSERVATION_H
#define OBSERVATION_H

#include "Rect.h"
#include "SceneObject.h"

//class SceneObject;

struct SceneObjectWithObservation;

struct Observation
{
	Observation(unsigned int timestamp, Rect rectangle, SceneObjectWithObservation* obj)
	: mTimestamp(timestamp)
	, mRectangle(rectangle)
	, mObject(obj)
	{

	}
	unsigned int mTimestamp;
	Rect mRectangle;
	SceneObjectWithObservation* mObject;
};


struct SceneObjectWithObservation
{
	SceneObject* mSceneObject;
	std::vector<Observation*> mObservations;
};




#endif